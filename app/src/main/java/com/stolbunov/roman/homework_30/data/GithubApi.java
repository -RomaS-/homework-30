package com.stolbunov.roman.homework_30.data;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GithubApi {

    private OkHttpClient client;

    @Inject
    public GithubApi(OkHttpClient client) {
        this.client = client;
    }

    public Single<List<UserGithub>> getUsers() {
        return Single.fromCallable(this::getUsersGitHub)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private List<UserGithub> getUsersGitHub() throws IOException {
        Request request = new Request.Builder()
                .url("https://api.github.com/repositories")
                .build();

        Call call = client.newCall(request);

        Response response = call.execute();
        String jsonResponse = response.body().string();

        Gson gson = new Gson();
        Type listType = new TypeToken<List<UserGithub>>() {
        }.getType();

        return gson.fromJson(jsonResponse, listType);
    }
}
