package com.stolbunov.roman.homework_30.data;

import com.stolbunov.roman.homework_30.domain.User;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

public class Mapper {

    @Inject
    public Mapper() {
    }

    public List<User> transform(List<UserGithub> userGithub) {
        List<User> users = new LinkedList<>();
        for (UserGithub github : userGithub) {
            List<String> nameTransform = transformFullName(github);
            users.add(new User(nameTransform.get(0), nameTransform.get(1)));
        }
        return users;
    }

    private List<String> transformFullName(UserGithub userGithub) {
        List<String> list = new ArrayList<>();
        String[] str;
        String delimiter = "/";
        String fullName = userGithub.getName();
        str = fullName.split(delimiter);
        for (String aStr : str) {
            list.add(aStr.substring(0, 1).toUpperCase() + aStr.substring(1));
        }
        return list;
    }
}
