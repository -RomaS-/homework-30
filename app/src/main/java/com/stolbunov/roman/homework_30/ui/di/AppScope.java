package com.stolbunov.roman.homework_30.ui.di;

import javax.inject.Scope;

@Scope
public @interface AppScope {
}
