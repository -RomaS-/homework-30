package com.stolbunov.roman.homework_30.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.homework_30.R;
import com.stolbunov.roman.homework_30.domain.User;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder> {
    private List<User> data;

    @Inject
    public UserAdapter() {
        this.data = Collections.emptyList();
    }

    public void setData(List<User> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_user, viewGroup, false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder userHolder, int position) {
        userHolder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.user_name)
        AppCompatTextView textView;

        UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(User user) {
            textView.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
        }
    }
}
