package com.stolbunov.roman.homework_30.ui.di;

import com.stolbunov.roman.homework_30.ui.activity.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = AndroidSupportInjectionModule.class)
public interface DaggerAndroidModule {

    @ContributesAndroidInjector
    MainActivity mainActivity();
}
