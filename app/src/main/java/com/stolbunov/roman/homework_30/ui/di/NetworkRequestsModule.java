package com.stolbunov.roman.homework_30.ui.di;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class NetworkRequestsModule {

    @AppScope
    @Provides
    OkHttpClient provideOnHttpClient() {
        return new OkHttpClient();
    }
}
