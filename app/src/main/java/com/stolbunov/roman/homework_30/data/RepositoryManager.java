package com.stolbunov.roman.homework_30.data;

import android.annotation.SuppressLint;

import com.stolbunov.roman.homework_30.domain.User;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class RepositoryManager {
    private GithubApi githubApi;
    private Mapper mapper;

    @Inject
    public RepositoryManager(GithubApi githubApi, Mapper mapper) {
        this.githubApi = githubApi;
        this.mapper = mapper;
    }

    @SuppressLint("CheckResult")
    public Single<List<User>> getUsers() {
        return githubApi.getUsers().map(res -> mapper.transform(res));
    }
}
