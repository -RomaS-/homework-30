package com.stolbunov.roman.homework_30.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.stolbunov.roman.homework_30.R;
import com.stolbunov.roman.homework_30.data.RepositoryManager;
import com.stolbunov.roman.homework_30.domain.User;
import com.stolbunov.roman.homework_30.ui.adapter.UserAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.disposables.Disposable;

public class MainActivity extends DaggerAppCompatActivity {
    private final String TAG = "ALevel";

    @Inject
    RepositoryManager repositoryManager;

    @Inject
    UserAdapter adapter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.loadingProgress)
    View loadingProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        adapter = new UserAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));


        Disposable subscribe = repositoryManager.getUsers().subscribe(this::onSussesRequest, this::onErrorRequest);
    }

    private void onErrorRequest(Throwable throwable) {
        Log.e(TAG, "onErrorRequest: " + throwable.getMessage());
    }

    private void onSussesRequest(List<User> users) {
        adapter.setData(users);
        loadingProgress.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }


}
