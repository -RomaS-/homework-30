package com.stolbunov.roman.homework_30.data;

import com.google.gson.annotations.SerializedName;

public class UserGithub {
    @SerializedName("full_name")
    String name;

    public String getName() {
        return name;
    }
}
