package com.stolbunov.roman.homework_30.ui.di;

import com.stolbunov.roman.homework_30.ui.App;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

@AppScope
@Component(modules = {DaggerAndroidModule.class, NetworkRequestsModule.class})
public interface AppComponent extends AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        AndroidInjector<? extends DaggerApplication> build();
    }
}
